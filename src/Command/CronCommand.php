<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CronCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('parse:cron')
            // the short description shown while running "php bin/console list"
            ->setDescription('Parse news')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows you to parse news by cron...");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $process = new Process('bin/console parse:work-ua --pages=10');
        try {
            $process->mustRun();

            echo $process->getOutput();
        } catch (ProcessFailedException $e) {
            echo $e->getMessage();
        }

        $process = new Process('bin/console parse:rabota-ua --pages=10');
        $process->start();

        $process = new Process('bin/console parse:hh-ua --pages=10');
        $process->start();
    }
}
